'use strict';

var React = require('react-native');
var QRCode = require('react-native-qrcode');
import BarcodeScanner from 'react-native-barcodescanner';
import Store from 'react-native-store';

var store = require('react-native-simple-store');

var {
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    View,
    Navigator,
    TouchableOpacity,
    } = React;

var SCREEN_WIDTH = require('Dimensions').get('window').width;
var SCREEN_HEIGHT = require('Dimensions').get('window').height;

var BaseConfig = Navigator.SceneConfigs.FloatFromRight;

var CustomLeftToRightGesture = Object.assign({}, BaseConfig.gestures.pop, {
    // Make it snap back really quickly after canceling pop
    snapVelocity: 8,
    // Make it so we can drag anywhere on the screen
    edgeHitWidth: SCREEN_WIDTH,
});

var CustomSceneConfig = Object.assign({}, BaseConfig, {
    // A very tighly wound spring will make this transition fast
    springTension: 100,
    springFriction: 1,
    // Use our custom gesture defined above
    gestures: {
        pop: CustomLeftToRightGesture,
    }
});

var EditScreen = React.createClass({
    componentDidMount() {
        store.get('PROFILE').then((profile) => {
            this.setState({name: profile.name, address: profile.address, email: profile.email});
        });
    },
    save() {
        store.save('PROFILE', {
            name: this.state.name,
            address: this.state.address,
            email: this.state.email
        }).then(() => {
            this.props.navigator.pop();
        });
    },
    getInitialState: function () {
        return {
            name: "",
            address: "",
            email: ""
        };
    },

    render() {
        return (
            <View style={[styles.container, {backgroundColor: 'green'}]}>
                <Text style={styles.welcome}>Here will be your Contacts!</Text>
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                    onChangeText={(text) => this.setState({name: text})}
                    value={this.state.name}
                    />
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                    onChangeText={(text) => this.setState({address: text})}
                    value={this.state.address}
                    />
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                    onChangeText={(text) => this.setState({email: text})}
                    value={this.state.email}
                    />
                <TouchableOpacity onPress={this.save}>
                    <View style={{paddingVertical: 10, paddingHorizontal: 20, backgroundColor: 'black'}}>
                        <Text style={styles.welcome}>Save</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    },
});

var ProfileScreen = React.createClass({
    componentDidMount() {
        store.get('PROFILE').then((profile) => {
            this.setState({name: profile.name, address: profile.address, email: profile.email});
        });
    },
    send() {
        this.props.navigator.push({id: 3,});
    }, edit() {
        this.props.navigator.push({id: 2,});
    },
    getInitialState: function () {
        return {
            name: 'name',
            address: 'address',
            email: 'email',
        };
    },

    render() {
        return (
            <View style={[styles.container, {backgroundColor: 'green'}]}>
                <Text>{this.state.name}</Text>
                <Text>{this.state.address}</Text>
                <Text>{this.state.email}</Text>

                <TouchableOpacity onPress={this._handlePress}>
                    <View style={{paddingVertical: 10, paddingHorizontal: 20, backgroundColor: 'black'}}>
                        <Text style={styles.welcome}>Save</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.edit}>
                    <View style={{paddingVertical: 10, paddingHorizontal: 20, backgroundColor: 'black'}}>
                        <Text style={styles.welcome}>Edit</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    },
});


var ScanQR = React.createClass({
    generateKey(){
        var diffie = crypto.createDiffieHellman(this.state.text);
        return diffie.generateKeys();
    },
    calculateSecret(){
        var diffie = crypto.createDiffieHellman(this.state.text);
        return diffie.computeSecret(this.state.qr);
    },
    getInitialState: function () {
        return {
            torchMode: 'off',
            cameraType: 'back',
        };
    },
    barcodeReceived(e) {

    },

    render() {
        return (
            <BarcodeScanner
                onBarCodeRead={this.barcodeReceived}
                style={{ flex: 1 }}
                torchMode={this.state.torchMode}
                cameraType={this.state.cameraType}
                />
        );
    }
});

var GenerateQR = React.createClass({
    getInitialState: function () {
        return {
            text: '',
        };
    },
    _handlePress() {

    },
    render: function () {
        return (
            <View style={[styles.container, {backgroundColor: 'blue'}]}>
                <Text style={styles.welcome}>Please Input a prime number, your contact should input the same one</Text>
                <TextInput
                    style={styles.input}
                    onChangeText={(text) => this.setState({text: text})}
                    value={this.state.text}
                    />
                <QRCode
                    value={generateKey()}
                    size={200}
                    bgColor='purple'
                    fgColor='white'/>

                <TouchableOpacity onPress={this._handlePress}>
                    <View style={{paddingVertical: 10, paddingHorizontal: 20, backgroundColor: 'black'}}>
                        <Text style={styles.welcome}>Go back</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
});

var CryptoMessenger = React.createClass({
    _renderScene(route, navigator) {
        if (route.id === 1) {
            return <ProfileScreen navigator={navigator}/>
        } else if (route.id === 2) {
            return <EditScreen navigator={navigator}/>
        } else if (route.id === 3) {
            return <ScanQR navigator={navigator}/>
        } else if (route.id === 4) {
            return <GenerateQR navigator={navigator}/>
        }
    },

    _configureScene(route) {
        return CustomSceneConfig;
    },

    render() {
        return (
            <Navigator
                initialRoute={{id: 1, }}
                renderScene={this._renderScene}
                configureScene={this._configureScene}/>
        );
    }
});

var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "transparent",
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: 'white',
    }, input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        margin: 10,
        borderRadius: 5,
        padding: 5,
        color: 'white',
        fontSize: 20,
    }, preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: SCREEN_HEIGHT,
        width: SCREEN_WIDTH
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        color: '#000',
        padding: 10,
        margin: 40
    }
});

AppRegistry.registerComponent('CryptoMessenger', () => CryptoMessenger);

module.exports = CryptoMessenger;